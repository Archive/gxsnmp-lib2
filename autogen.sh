#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="gxsnmp-lib"

(test -f lib/gxsnmp_hash.c) || {
	echo "You must run this script in the top-level gxsnmp-lib directory"
	exit 1
}

USE_GNOME2_MACROS=1 . gnome-autogen.sh

