#include <string.h>
#include <stdio.h>
#include <glib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "gxsnmp_message.h"
#include "gxsnmp_dgram.h"

struct IPv4Address {
  socklen_t          len;
  struct sockaddr_in addr;
};

static GMainLoop *main_loop;

static int
timeout_cb (gpointer data)
{
  printf("Request timed out\n");
  g_main_quit(main_loop);
}

static gboolean
recv_message (GxsnmpIOMessage *message, GIOCondition cond, gpointer data)
{
  char *buf;
  int count;
  struct IPv4Address address;
  guint i;
  GIOError error;

  g_main_quit(main_loop);
  if (cond & (G_IO_ERR | G_IO_HUP))
    {
      printf("Received error\n");
    }

  if (cond & G_IO_IN)
    {
      error = gxsnmp_io_message_recv (message, &buf, &count, 
		      		     (GxsnmpIOAddress) &address);
      if (error != G_IO_ERROR_NONE)
	{
	  printf("Received IO error\n");
	  return ;
	}
      printf ("Received reponse (len=%d)\n", count);
      for (i=0; i<count; i++)
	{
	  if (i % 16 == 0)
            g_print ("%8.8x:", i);
          if (i % 4 == 0)
	    g_print (" ");
          g_print ("%2.2x", ((guchar *)buf)[i]);
          if (i % 16 == 15)
	    g_print ("\n");
	}
        if (i % 16 != 15)
	  g_print ("\n");

      g_free(buf);
    }
  return FALSE;
}

int
main(int argc, char *argv[])
{
  gint fd;
  GxsnmpIOMessage *my_message;
  struct IPv4Address address;
  struct hostent *hp;
  struct servent *port;

  guint *id;

  if (argc != 2) 
    {
       printf("Usage: %s target\n", argv[0]);
       return 1;
    }

  hp = gethostbyname(argv[1]);
  if (!hp)
    {
      printf("Can't resolve %s\n", argv[1]);
      return 1;
    }

  port = getservbyname("echo", "udp");
  if (!port)
    {
      printf("Can't get port for echo/udp\n");
    }

  address.len = sizeof(struct sockaddr_in);
  g_memmove(&address.addr.sin_addr, (char *)hp->h_addr, hp->h_length);
  address.addr.sin_port = port->s_port;
  address.addr.sin_family  = AF_INET;

  fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (fd == -1)
    {
      printf("Can't get IP socket\n");
    }

  my_message = gxsnmp_io_message_dgram_new (fd);

  id = g_new (guint, 1);
  *id = gxsnmp_io_madd_watch (my_message, 
		  	      G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP,
	                      recv_message, id);

  g_timeout_add(5000, timeout_cb, NULL);
  gxsnmp_io_message_send(my_message, "hallo", 5, &address);
  main_loop = g_main_new (FALSE);
  g_main_run (main_loop);
  return 0;
}
