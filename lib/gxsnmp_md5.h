/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 *
 * This code implements the MD5 message-digest algorithm.
 * The algorithm is due to Ron Rivest.  This code was
 * written by Colin Plumb in 1993, no copyright is claimed.
 * This code is in the public domain; do with it what you wish.
 *
 * Equivalent code is available from RSA Data Security, Inc.
 * This code has been tested against that, and is equivalent,
 * except that you don't need to include two pages of legalese
 * with every copy.
 */
#ifndef __GXSNMP_MD5_H__
#define __GXSNMP_MD5_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GxsnmpMd5      GxsnmpMd5;
typedef struct _GxsnmpMd5Class GxsnmpMd5Class;

#define GXSNMP_TYPE_MD5                (gxsnmp_md5_get_type ())
#define GXSNMP_MD5(object)             (G_TYPE_CHECK_INSTANCE_CAST ((object), GXSNMP_TYPE_MD5, GxsnmpMd5))
#define GXSNMP_MD5_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_MD5, GxsnmpMd5Class))
#define GXSNMP_IS_MD5(object)          (G_TYPE_CHECK_INSTANCE_TYPE ((object), GXSNMP_TYPE_MD5))
#define GXSNMP_IS_MD5_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MD5))
#define GXSNMP_MD5_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GXSNMP_TYPE_MD5, GxsnmpMd5Class))


struct _GxsnmpMd5
{
  GxsnmpDigest parent_instance;

  /*< public >*/

  /*< private >*/
};

struct _GxsnmpMd5Class
{
  GxsnmpDigestClass parent_class;

};

GType        gxsnmp_md5_get_type (void) G_GNUC_CONST;

GxsnmpMd5*   gxsnmp_md5_new	 ();


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
