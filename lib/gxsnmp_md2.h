/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 *
 * This code implements the MD2 message-digest algorithm.
 */
#ifndef __GXSNMP_MD2_H__
#define __GXSNMP_MD2_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GxsnmpMd2      GxsnmpMd2;
typedef struct _GxsnmpMd2Class GxsnmpMd2Class;

#define GXSNMP_TYPE_MD2                (gxsnmp_md2_get_type ())
#define GXSNMP_MD2(object)             (G_TYPE_CHECK_INSTANCE_CAST ((object), GXSNMP_TYPE_MD2, GxsnmpMd2))
#define GXSNMP_MD2_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_MD2, GxsnmpMd2Class))
#define GXSNMP_IS_MD2(object)          (G_TYPE_CHECK_INSTANCE_TYPE ((object), GXSNMP_TYPE_MD2))
#define GXSNMP_IS_MD2_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MD2))
#define GXSNMP_MD2_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GXSNMP_TYPE_MD2, GxsnmpMd2Class))


struct _GxsnmpMd2
{
  GxsnmpHash parent_instance;

  /*< public >*/

  /*< private >*/
  guchar data[16];
  guchar checksum[16];
  guint  count;
};

struct _GxsnmpMd2Class
{
  GxsnmpHashClass parent_class;

};

GType        gxsnmp_md2_get_type (void) G_GNUC_CONST;

GxsnmpMd2*   gxsnmp_md2_new	 ();


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
