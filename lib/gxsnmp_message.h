/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GxsnmpIOMessage
 */

typedef struct _GxsnmpIOMFuncs  GxsnmpIOMFuncs;
typedef struct _GxsnmpIOMessage GxsnmpIOMessage;

typedef gpointer GxsnmpIOAddress;

struct _GxsnmpIOMessage
{
  guint message_flags;
  guint ref_count;
  GxsnmpIOMFuncs *funcs;
};

typedef gboolean (*GxsnmpIOMFunc) (GxsnmpIOMessage   *source,
                                   GIOCondition       condition,
                                   gpointer           data);

struct _GxsnmpIOMFuncs
{
  GIOError (*io_recv)   (GxsnmpIOMessage 	 *message, 
		         gchar			**buf, 
		         guint			 *count,
			 GxsnmpIOAddress	  sender);
  GIOError (*io_send)   (GxsnmpIOMessage 	 *message, 
		 	 gchar     	 	 *buf, 
			 guint      		  count,
			 GxsnmpIOAddress	  recipient);
  void (*io_close)      (GxsnmpIOMessage	 *message);
  GSource * (*io_create_watch) (GxsnmpIOMessage	 *message,
			 GIOCondition   	  condition);
  void (*io_free)       (GxsnmpIOMessage	 *message);
};

void       gxsnmp_io_message_init    (GxsnmpIOMessage    *message);
void       gxsnmp_io_message_ref     (GxsnmpIOMessage    *message);
void       gxsnmp_io_message_unref   (GxsnmpIOMessage    *message);
GIOError   gxsnmp_io_message_recv    (GxsnmpIOMessage    *message, 
				      gchar     	**buf, 
			       	      guint    	   	 *count,
				      GxsnmpIOAddress  	  sender);
GIOError   gxsnmp_io_message_send    (GxsnmpIOMessage    *message, 
				      gchar		 *buf, 
				      guint		  count,
				      GxsnmpIOAddress     recipient);
void       gxsnmp_io_message_close   (GxsnmpIOMessage    *message);
guint      gxsnmp_io_madd_watch_full (GxsnmpIOMessage    *message,
				      gint         	  priority,
				      GIOCondition 	  condition,
			 	      GxsnmpIOMFunc       func,
			 	      gpointer 	   	  user_data,
				      GDestroyNotify      notify);
guint      gxsnmp_io_madd_watch      (GxsnmpIOMessage    *message,
				      GIOCondition 	  condition,
			              GxsnmpIOMFunc       func,
			              gpointer  	  user_data);


