/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 * Copyright (C) 2001 Gregory McLean & Jochen Friedrich
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <gobject/gobject.h>
#include "gxsnmp_hash.h"

static void gxsnmp_hash_init       (GxsnmpHash      *hash);
static void gxsnmp_hash_class_init (GxsnmpHashClass *klass);
static void gxsnmp_hash_finalize   (GObject         *object);

static void gxsnmp_hash_update1    (GxsnmpHash      *hash,
				    guchar const    *buf,
				    guint	     len);
static void gxsnmp_hash_final1     (GxsnmpHash      *hash,
	       			    guchar	    *buf);
static void gxsnmp_hash_reset1	   (GxsnmpHash	    *hash);

static gpointer parent_class = NULL;

GType
gxsnmp_hash_get_type (void)
{
  static GType object_type = 0;

  if (!object_type)
    {
      static const GTypeInfo object_info =
        {
          sizeof (GxsnmpHashClass),
          (GBaseInitFunc) NULL,
          (GBaseFinalizeFunc) NULL,
          (GClassInitFunc) gxsnmp_hash_class_init,
          NULL,           /* class_finalize */
          NULL,           /* class_data */
          sizeof (GxsnmpHash),
          0,              /* n_preallocs */
          (GInstanceInitFunc) gxsnmp_hash_init,
        };

        object_type = g_type_register_static (G_TYPE_OBJECT,
                                              "GxsnmpHash",
                                              &object_info, 0);
    }
  return object_type;
}

static void
gxsnmp_hash_init (GxsnmpHash *hash)
{
  hash->hashlen   = 0; /* Virtual */
  hash->hashname  = "NONE";

  hash->result    = NULL;
}

static void
gxsnmp_hash_class_init (GxsnmpHashClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  klass->update    = gxsnmp_hash_update1;
  klass->final     = gxsnmp_hash_final1;
  klass->reset     = gxsnmp_hash_reset1;

  object_class->finalize = gxsnmp_hash_finalize;
}

static void
gxsnmp_hash_finalize (GObject *object)
{
  GxsnmpHash *hash = GXSNMP_HASH (object);

  if (hash->result) 
    g_free(hash->result);
  hash->result = NULL;

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gxsnmp_hash_update1 (GxsnmpHash *hash, guchar const *buf, guint len)
{
}

static void
gxsnmp_hash_final1 (GxsnmpHash *hash, guchar *buf)
{
}

static void
gxsnmp_hash_reset1 (GxsnmpHash *hash)
{
}

void
gxsnmp_hash_update (GxsnmpHash *hash, guchar const *buf, guint len)
{
  GxsnmpHashClass *class;

  g_return_if_fail (hash != NULL);
  g_return_if_fail (GXSNMP_IS_HASH(hash));

  class = GXSNMP_HASH_GET_CLASS (hash);

  if (class->update)
    class->update (hash, buf, len);
}

void
gxsnmp_hash_final (GxsnmpHash *hash, guchar *buf)
{
  GxsnmpHashClass *class;

  g_return_if_fail (hash != NULL);
  g_return_if_fail (GXSNMP_IS_HASH(hash));

  class = GXSNMP_HASH_GET_CLASS (hash);

  if (class->final)
    class->final (hash, buf);
}

void
gxsnmp_hash_reset (GxsnmpHash *hash)
{
  GxsnmpHashClass *class;

  g_return_if_fail (hash != NULL);
  g_return_if_fail (GXSNMP_IS_HASH(hash));

  class = GXSNMP_HASH_GET_CLASS (hash);

  if (class->reset)
    class->reset (hash);
}

void
gxsnmp_hash_pw2key (GxsnmpHash *hash, guchar *pw, guint pwlen, guchar *out)
{
  guchar *cp, pw_buf[64];
  gulong  pw_index = 0;
  gulong  count = 0, i;

  /**********************************************/
  /* Use while loop until we've done 1 Megabyte */
  /**********************************************/

  while (count < 1048576)
    {
      cp = pw_buf;
      for(i = 0; i < 64; i++) 
        {
          /*************************************************/
          /* Take the next octet of the password, wrapping */
          /* to the beginning of the password as necessary.*/
          /*************************************************/
          *cp++ = pw[ pw_index++ % pwlen ];
        }
      gxsnmp_hash_update(hash, pw_buf, 64);
      count += 64;
    }
  gxsnmp_hash_final (hash, out);              /* tell HASH we're done */
}

void
gxsnmp_hash_localize (GxsnmpHash *hash, guchar *key, 
		      guchar *engine, guint enginelen)
{
  guchar *password_buf;
  gulong  len;

  len = enginelen + hash->hashlen * 2;
  password_buf = g_malloc(len);

  g_memmove (password_buf, key, hash->hashlen);
  g_memmove (password_buf+hash->hashlen, engine, enginelen);
  g_memmove (password_buf+hash->hashlen+enginelen, key, hash->hashlen);

  gxsnmp_hash_update (hash, password_buf, hash->hashlen * 2 +enginelen);
  gxsnmp_hash_final (hash,key);

  g_free(password_buf);
}
