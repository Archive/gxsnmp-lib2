/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 *
 */

#ifndef __GXSNMP_RIPEMD160_H__
#define __GXSNMP_RIPEMD160_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GxsnmpRipemd160      GxsnmpRipemd160;
typedef struct _GxsnmpRipemd160Class GxsnmpRipemd160Class;

#define GXSNMP_TYPE_RIPEMD160                (gxsnmp_ripemd160_get_type ())
#define GXSNMP_RIPEMD160(object)             (G_TYPE_CHECK_INSTANCE_CAST ((object), GXSNMP_TYPE_RIPEMD160, GxsnmpRipemd160))
#define GXSNMP_RIPEMD160_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_RIPEMD160, GxsnmpRipemd160Class))
#define GXSNMP_IS_RIPEMD160(object)          (G_TYPE_CHECK_INSTANCE_TYPE ((object), GXSNMP_TYPE_RIPEMD160))
#define GXSNMP_IS_RIPEMD160_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_RIPEMD160))
#define GXSNMP_RIPEMD160_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GXSNMP_TYPE_RIPEMD160, GxsnmpRipemd160Class))


struct _GxsnmpRipemd160
{
  GxsnmpDigest parent_instance;

  /*< public >*/

  /*< private >*/
};

struct _GxsnmpRipemd160Class
{
  GxsnmpDigestClass parent_class;

};

GType        gxsnmp_ripemd160_get_type (void) G_GNUC_CONST;

GxsnmpRipemd160*   gxsnmp_ripemd160_new	 ();


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
