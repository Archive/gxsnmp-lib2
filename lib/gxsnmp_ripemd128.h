/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 *
 */

#ifndef __GXSNMP_RIPEMD128_H__
#define __GXSNMP_RIPEMD128_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GxsnmpRipemd128      GxsnmpRipemd128;
typedef struct _GxsnmpRipemd128Class GxsnmpRipemd128Class;

#define GXSNMP_TYPE_RIPEMD128                (gxsnmp_ripemd128_get_type ())
#define GXSNMP_RIPEMD128(object)             (G_TYPE_CHECK_INSTANCE_CAST ((object), GXSNMP_TYPE_RIPEMD128, GxsnmpRipemd128))
#define GXSNMP_RIPEMD128_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_RIPEMD128, GxsnmpRipemd128Class))
#define GXSNMP_IS_RIPEMD128(object)          (G_TYPE_CHECK_INSTANCE_TYPE ((object), GXSNMP_TYPE_RIPEMD128))
#define GXSNMP_IS_RIPEMD128_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_RIPEMD128))
#define GXSNMP_RIPEMD128_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GXSNMP_TYPE_RIPEMD128, GxsnmpRipemd128Class))


struct _GxsnmpRipemd128
{
  GxsnmpDigest parent_instance;

  /*< public >*/

  /*< private >*/
};

struct _GxsnmpRipemd128Class
{
  GxsnmpDigestClass parent_class;

};

GType        gxsnmp_ripemd128_get_type (void) G_GNUC_CONST;

GxsnmpRipemd128*   gxsnmp_ripemd128_new	 ();


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
