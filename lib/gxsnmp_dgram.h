/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * Datagram Messages
 */

typedef struct _GxsnmpIODgramMessage GxsnmpIODgramMessage;
typedef struct _GxsnmpIODgramWatch GxsnmpIODgramWatch;
typedef struct _GxsnmpIODgramAddress GxsnmpIODgramAddress;

struct _GxsnmpIODgramMessage {
  GxsnmpIOMessage message;
  gint fd;
};

struct _GxsnmpIODgramWatch {
  GSource	     source;
  GPollFD  	     pollfd;
  GxsnmpIOMessage   *message;
  GIOCondition	     condition;
  GxsnmpIOMFunc      callback;
};

struct _GxsnmpIODgramAddress {
  socklen_t	  len;
  struct sockaddr addr;
};

GxsnmpIOMessage * gxsnmp_io_message_dgram_new (gint fd);

