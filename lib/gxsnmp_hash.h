/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 * Copyright (C) 2001 Gregory McLean & Jochen Friedrich
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#ifndef __GXSNMP_HASH_H__
#define __GXSNMP_HASH_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GxsnmpHash      GxsnmpHash;
typedef struct _GxsnmpHashClass GxsnmpHashClass;

#define GXSNMP_TYPE_HASH               (gxsnmp_hash_get_type ())
#define GXSNMP_HASH(object)            (G_TYPE_CHECK_INSTANCE_CAST ((object), GXSNMP_TYPE_HASH, GxsnmpHash))
#define GXSNMP_HASH_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_HASH, GxsnmpHashClass))
#define GXSNMP_IS_HASH(object)         (G_TYPE_CHECK_INSTANCE_TYPE ((object), GXSNMP_TYPE_HASH))
#define GXSNMP_IS_HASH_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_HASH))
#define GXSNMP_HASH_GET_CLASS(obj)     (G_TYPE_INSTANCE_GET_CLASS ((obj), GXSNMP_TYPE_HASH, GxsnmpHashClass))


struct _GxsnmpHash
{
  GObject  parent_instance;

  /*< public >*/
  guint    hashlen;
  guchar  *hashname;

  /*< private >*/
  guint32 *result;
};

struct _GxsnmpHashClass
{
  GObjectClass parent_class;

  /* Class methods to calculate hash values */
  void (*update)  (GxsnmpHash *hash, guchar const *buf, guint len);
  void (*final)   (GxsnmpHash *hash, guchar *buf);
  void (*reset)   (GxsnmpHash *hash);
};

GType        gxsnmp_hash_get_type (void) G_GNUC_CONST;

void gxsnmp_hash_update   (GxsnmpHash *hash, guchar const *buf, guint len);
void gxsnmp_hash_final    (GxsnmpHash *hash, guchar *buf);
void gxsnmp_hash_reset    (GxsnmpHash *hash);
void gxsnmp_hash_pw2key   (GxsnmpHash *hash, guchar *pw, guint pwlen, 
		           guchar *out);
void gxsnmp_hash_localize (GxsnmpHash *hash, guchar *key, 
			   guchar *engine, guint enginelen);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
