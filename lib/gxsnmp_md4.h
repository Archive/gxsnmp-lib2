/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 *
 * This code implements the MD4 message-digest algorithm.
 */
#ifndef __GXSNMP_MD4_H__
#define __GXSNMP_MD4_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GxsnmpMd4      GxsnmpMd4;
typedef struct _GxsnmpMd4Class GxsnmpMd4Class;

#define GXSNMP_TYPE_MD4                (gxsnmp_md4_get_type ())
#define GXSNMP_MD4(object)             (G_TYPE_CHECK_INSTANCE_CAST ((object), GXSNMP_TYPE_MD4, GxsnmpMd4))
#define GXSNMP_MD4_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_MD4, GxsnmpMd4Class))
#define GXSNMP_IS_MD4(object)          (G_TYPE_CHECK_INSTANCE_TYPE ((object), GXSNMP_TYPE_MD4))
#define GXSNMP_IS_MD4_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_MD4))
#define GXSNMP_MD4_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GXSNMP_TYPE_MD4, GxsnmpMd4Class))


struct _GxsnmpMd4
{
  GxsnmpDigest parent_instance;

  /*< public >*/

  /*< private >*/
};

struct _GxsnmpMd4Class
{
  GxsnmpDigestClass parent_class;

};

GType        gxsnmp_md4_get_type (void) G_GNUC_CONST;

GxsnmpMd4*   gxsnmp_md4_new	 ();


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
