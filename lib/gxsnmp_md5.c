/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 *
 * This code implements the MD5 message-digest algorithm.
 * The algorithm is due to Ron Rivest.  This code was
 * written by Colin Plumb in 1993, no copyright is claimed.
 * This code is in the public domain; do with it what you wish.
 *
 * Equivalent code is available from RSA Data Security, Inc.
 * This code has been tested against that, and is equivalent,
 * except that you don't need to include two pages of legalese
 * with every copy.
 *
 * To compute the message digest of a chunk of bytes, declare an
 * MD5Context structure, pass it to MD5Init, call MD5Update as
 * needed on buffers full of bytes, and then call MD5Final, which
 * will fill a supplied 16-byte array with the digest.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <gobject/gobject.h>
#include "gxsnmp_hash.h"
#include "gxsnmp_digest.h"
#include "gxsnmp_md5.h"

static void MD5Init(guint32 digest[4]);
static void MD5Transform(guint32 digest[4], guint32 const data[16]);

static void gxsnmp_md5_init        (GxsnmpMd5       *md5);
static void gxsnmp_md5_class_init  (GxsnmpMd5Class  *klass);
static void gxsnmp_md5_finalize    (GObject         *object);

static void gxsnmp_md5_transform   (GxsnmpDigest    *digest);
static void gxsnmp_md5_reset       (GxsnmpHash      *hash);

static gpointer parent_class = NULL;

GType
gxsnmp_md5_get_type (void)
{
  static GType object_type = 0;

  if (!object_type)
    {
      static const GTypeInfo object_info =
        {
          sizeof (GxsnmpMd5Class),
          (GBaseInitFunc) NULL,
          (GBaseFinalizeFunc) NULL,
          (GClassInitFunc) gxsnmp_md5_class_init,
          NULL,           /* class_finalize */
          NULL,           /* class_data */
          sizeof (GxsnmpMd5),
          0,              /* n_preallocs */
          (GInstanceInitFunc) gxsnmp_md5_init,
        };

        object_type = g_type_register_static (gxsnmp_digest_get_type(),
                                              "GxsnmpMd5",
                                              &object_info, 0);
    }
  return object_type;
}

static void
gxsnmp_md5_init (GxsnmpMd5 *object)
{
  GxsnmpHash   *hash   = GXSNMP_HASH(object);
  GxsnmpDigest *digest = GXSNMP_DIGEST(object);

  hash->result    = g_malloc(16);
  hash->hashlen   = 16;
  hash->hashname  = "MD5";
  digest->endianess = FALSE;
  if (hash->result)
    MD5Init(hash->result);
}

static void
gxsnmp_md5_class_init (GxsnmpMd5Class *klass)
{
  GObjectClass      *object_class = G_OBJECT_CLASS (klass);
  GxsnmpDigestClass *digest_class = GXSNMP_DIGEST_CLASS (klass);
  GxsnmpHashClass   *hash_class   = GXSNMP_HASH_CLASS (klass);
  parent_class = g_type_class_peek_parent (klass);

  digest_class->transform = gxsnmp_md5_transform;
  hash_class->reset       = gxsnmp_md5_reset;
  object_class->finalize  = gxsnmp_md5_finalize;
}

static void
gxsnmp_md5_finalize (GObject *object)
{
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gxsnmp_md5_reset (GxsnmpHash *hash)
{
  GXSNMP_HASH_CLASS (parent_class)->reset (hash);

  if (hash->result)
    MD5Init(hash->result);
}

static void
gxsnmp_md5_transform (GxsnmpDigest *digest)
{
  GxsnmpHash *hash = GXSNMP_HASH (digest);

  if (hash->result)
    MD5Transform(hash->result, digest->data);
}

/*
 * Start MD5 accumulation.  Set bit count to 0 and buffer to mysterious
 * initialization constants.
 */
static void 
MD5Init(guint32 digest[4])
{
  digest[0] = 0x67452301;
  digest[1] = 0xefcdab89;
  digest[2] = 0x98badcfe;
  digest[3] = 0x10325476;
}

/* The four core functions - F1 is optimized somewhat */

/* #define F1(x, y, z) (x & y | ~x & z) */
#define F1(x, y, z) (z ^ (x & (y ^ z)))
#define F2(x, y, z) F1(z, x, y)
#define F3(x, y, z) (x ^ y ^ z)
#define F4(x, y, z) (y ^ (x | ~z))

/* This is the central step in the MD5 algorithm. */
#define MD5STEP(f, w, x, y, z, data, s) \
	( w += f(x, y, z) + data,  w = w<<s | w>>(32-s),  w += x )

/*
 * The core of the MD5 algorithm, this alters an existing MD5 hash to
 * reflect the addition of 16 longwords of new data.  MD5Update blocks
 * the data and converts bytes into longwords for this routine.
 */
static void 
MD5Transform(guint32 digest[4], guint32 const data[16])
{
  register guint32 a, b, c, d;

  a = digest[0];
  b = digest[1];
  c = digest[2];
  d = digest[3];

  MD5STEP(F1, a, b, c, d, data[0] + 0xd76aa478, 7);
  MD5STEP(F1, d, a, b, c, data[1] + 0xe8c7b756, 12);
  MD5STEP(F1, c, d, a, b, data[2] + 0x242070db, 17);
  MD5STEP(F1, b, c, d, a, data[3] + 0xc1bdceee, 22);
  MD5STEP(F1, a, b, c, d, data[4] + 0xf57c0faf, 7);
  MD5STEP(F1, d, a, b, c, data[5] + 0x4787c62a, 12);
  MD5STEP(F1, c, d, a, b, data[6] + 0xa8304613, 17);
  MD5STEP(F1, b, c, d, a, data[7] + 0xfd469501, 22);
  MD5STEP(F1, a, b, c, d, data[8] + 0x698098d8, 7);
  MD5STEP(F1, d, a, b, c, data[9] + 0x8b44f7af, 12);
  MD5STEP(F1, c, d, a, b, data[10] + 0xffff5bb1, 17);
  MD5STEP(F1, b, c, d, a, data[11] + 0x895cd7be, 22);
  MD5STEP(F1, a, b, c, d, data[12] + 0x6b901122, 7);
  MD5STEP(F1, d, a, b, c, data[13] + 0xfd987193, 12);
  MD5STEP(F1, c, d, a, b, data[14] + 0xa679438e, 17);
  MD5STEP(F1, b, c, d, a, data[15] + 0x49b40821, 22);

  MD5STEP(F2, a, b, c, d, data[1] + 0xf61e2562, 5);
  MD5STEP(F2, d, a, b, c, data[6] + 0xc040b340, 9);
  MD5STEP(F2, c, d, a, b, data[11] + 0x265e5a51, 14);
  MD5STEP(F2, b, c, d, a, data[0] + 0xe9b6c7aa, 20);
  MD5STEP(F2, a, b, c, d, data[5] + 0xd62f105d, 5);
  MD5STEP(F2, d, a, b, c, data[10] + 0x02441453, 9);
  MD5STEP(F2, c, d, a, b, data[15] + 0xd8a1e681, 14);
  MD5STEP(F2, b, c, d, a, data[4] + 0xe7d3fbc8, 20);
  MD5STEP(F2, a, b, c, d, data[9] + 0x21e1cde6, 5);
  MD5STEP(F2, d, a, b, c, data[14] + 0xc33707d6, 9);
  MD5STEP(F2, c, d, a, b, data[3] + 0xf4d50d87, 14);
  MD5STEP(F2, b, c, d, a, data[8] + 0x455a14ed, 20);
  MD5STEP(F2, a, b, c, d, data[13] + 0xa9e3e905, 5);
  MD5STEP(F2, d, a, b, c, data[2] + 0xfcefa3f8, 9);
  MD5STEP(F2, c, d, a, b, data[7] + 0x676f02d9, 14);
  MD5STEP(F2, b, c, d, a, data[12] + 0x8d2a4c8a, 20);

  MD5STEP(F3, a, b, c, d, data[5] + 0xfffa3942, 4);
  MD5STEP(F3, d, a, b, c, data[8] + 0x8771f681, 11);
  MD5STEP(F3, c, d, a, b, data[11] + 0x6d9d6122, 16);
  MD5STEP(F3, b, c, d, a, data[14] + 0xfde5380c, 23);
  MD5STEP(F3, a, b, c, d, data[1] + 0xa4beea44, 4);
  MD5STEP(F3, d, a, b, c, data[4] + 0x4bdecfa9, 11);
  MD5STEP(F3, c, d, a, b, data[7] + 0xf6bb4b60, 16);
  MD5STEP(F3, b, c, d, a, data[10] + 0xbebfbc70, 23);
  MD5STEP(F3, a, b, c, d, data[13] + 0x289b7ec6, 4);
  MD5STEP(F3, d, a, b, c, data[0] + 0xeaa127fa, 11);
  MD5STEP(F3, c, d, a, b, data[3] + 0xd4ef3085, 16);
  MD5STEP(F3, b, c, d, a, data[6] + 0x04881d05, 23);
  MD5STEP(F3, a, b, c, d, data[9] + 0xd9d4d039, 4);
  MD5STEP(F3, d, a, b, c, data[12] + 0xe6db99e5, 11);
  MD5STEP(F3, c, d, a, b, data[15] + 0x1fa27cf8, 16);
  MD5STEP(F3, b, c, d, a, data[2] + 0xc4ac5665, 23);

  MD5STEP(F4, a, b, c, d, data[0] + 0xf4292244, 6);
  MD5STEP(F4, d, a, b, c, data[7] + 0x432aff97, 10);
  MD5STEP(F4, c, d, a, b, data[14] + 0xab9423a7, 15);
  MD5STEP(F4, b, c, d, a, data[5] + 0xfc93a039, 21);
  MD5STEP(F4, a, b, c, d, data[12] + 0x655b59c3, 6);
  MD5STEP(F4, d, a, b, c, data[3] + 0x8f0ccc92, 10);
  MD5STEP(F4, c, d, a, b, data[10] + 0xffeff47d, 15);
  MD5STEP(F4, b, c, d, a, data[1] + 0x85845dd1, 21);
  MD5STEP(F4, a, b, c, d, data[8] + 0x6fa87e4f, 6);
  MD5STEP(F4, d, a, b, c, data[15] + 0xfe2ce6e0, 10);
  MD5STEP(F4, c, d, a, b, data[6] + 0xa3014314, 15);
  MD5STEP(F4, b, c, d, a, data[13] + 0x4e0811a1, 21);
  MD5STEP(F4, a, b, c, d, data[4] + 0xf7537e82, 6);
  MD5STEP(F4, d, a, b, c, data[11] + 0xbd3af235, 10);
  MD5STEP(F4, c, d, a, b, data[2] + 0x2ad7d2bb, 15);
  MD5STEP(F4, b, c, d, a, data[9] + 0xeb86d391, 21);

  digest[0] += a;
  digest[1] += b;
  digest[2] += c;
  digest[3] += d;
}

GxsnmpMd5*   gxsnmp_md5_new()
{
  GxsnmpMd5* md5;

  md5 = g_object_new (gxsnmp_md5_get_type (), NULL);

  return md5;
}
