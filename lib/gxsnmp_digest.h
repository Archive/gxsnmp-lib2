/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 * Copyright (C) 2001 Gregory McLean & Jochen Friedrich
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#ifndef __GXSNMP_DIGEST_H__
#define __GXSNMP_DIGEST_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GxsnmpDigest      GxsnmpDigest;
typedef struct _GxsnmpDigestClass GxsnmpDigestClass;

#define GXSNMP_TYPE_DIGEST               (gxsnmp_digest_get_type ())
#define GXSNMP_DIGEST(object)            (G_TYPE_CHECK_INSTANCE_CAST ((object), GXSNMP_TYPE_DIGEST, GxsnmpDigest))
#define GXSNMP_DIGEST_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_DIGEST, GxsnmpDigestClass))
#define GXSNMP_IS_DIGEST(object)         (G_TYPE_CHECK_INSTANCE_TYPE ((object), GXSNMP_TYPE_DIGEST))
#define GXSNMP_IS_DIGEST_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_DIGEST))
#define GXSNMP_DIGEST_GET_CLASS(obj)     (G_TYPE_INSTANCE_GET_CLASS ((obj), GXSNMP_TYPE_DIGEST, GxsnmpDigestClass))


struct _GxsnmpDigest
{
  GxsnmpHash parent_instance;

  /*< private >*/
  guint32  data[16];
  guint    countLo;
  guint    countHi;
  gboolean endianess;
};

struct _GxsnmpDigestClass
{
  GxsnmpHashClass parent_class;

  /* Class methods to calculate digest values */
  void (*transform) (GxsnmpDigest   *object);
};

GType        gxsnmp_digest_get_type (void) G_GNUC_CONST;

void gxsnmp_digest_hmac   (GxsnmpDigest *digest, guchar *data, guint datalen,
			   guchar *key, guint keylen, guchar *out);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
