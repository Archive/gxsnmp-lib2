#include <string.h>
#include <stdio.h>
#include <glib.h>
#include <gobject/gobject.h>

#include "gxsnmp_hash.h"
#include "gxsnmp_digest.h"
#include "gxsnmp_md2.h"
#include "gxsnmp_md4.h"
#include "gxsnmp_md5.h"
#include "gxsnmp_sha.h"
#include "gxsnmp_ripemd128.h"
#include "gxsnmp_ripemd160.h"

int
main(int argc, char *argv[])
{
  int i, cnt;
  guint j, num;
  guchar key[20];
  GxsnmpDigest *digest;
  GType  type;
  GType *tarray;

  g_type_init(G_TYPE_DEBUG_NONE);

  if (argc != 3) 
    {
       printf("Usage: %s text key\n", argv[0]);
       return 1;
    }

  /* Register classes */

  type = gxsnmp_md4_get_type();
  type = gxsnmp_md5_get_type();
  type = gxsnmp_sha_get_type();
  type = gxsnmp_ripemd128_get_type();
  type = gxsnmp_ripemd160_get_type();

  /* Get HASH type */
  type = gxsnmp_digest_get_type();

  tarray = g_type_children(type, &num);

  for (j=0;j<num;j++)
    {
      digest = g_object_new (tarray[j], NULL);
      gxsnmp_digest_hmac(digest, argv[1], strlen(argv[1]), 
		                 argv[2], strlen(argv[2]), key);
      printf("%s key : ", GXSNMP_HASH(digest)->hashname);
      for (i=0;i<GXSNMP_HASH(digest)->hashlen;i++) printf("%02x ", key[i]);
      printf("\n");
      g_object_unref(G_OBJECT(digest));
    }
  return 0;
}

