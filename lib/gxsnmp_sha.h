/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * GXSNMP -- An snmp mangament application
 *
 * SHA: NIST's Secure Hash Algorithm
 *
 * Based on SHA code originally posted to sci.crypt by Peter Gutmann
 * in message <30ajo5$oe8@ccu2.auckland.ac.nz>.
 * Modified to test for endianness on creation of SHA objects by AMK.
 * Also, the original specification of SHA was found to have a weakness
 * by NSA/NIST.  This code implements the fixed version of SHA.
 *
 * Here's the first paragraph of Peter Gutmann's posting:
 *    
 * The following is my SHA (FIPS 180) code updated to allow use of the "fixed"
 * SHA, thanks to Jim Gillogly and an anonymous contributor for the information
 * on what's changed in the new version.  The fix is a simple change which
 * involves adding a single rotate in the initial expansion function.  It is
 * unknown whether this is an optimal solution to the problem which was
 * discovered in the SHA or whether it's simply a bandaid which fixes the
 * problem with a minimum of effort (for example the reengineering of a great
 * many Capstone chips).
 */

#ifndef __GXSNMP_SHA_H__
#define __GXSNMP_SHA_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GxsnmpSha      GxsnmpSha;
typedef struct _GxsnmpShaClass GxsnmpShaClass;

#define GXSNMP_TYPE_SHA                (gxsnmp_sha_get_type ())
#define GXSNMP_SHA(object)             (G_TYPE_CHECK_INSTANCE_CAST ((object), GXSNMP_TYPE_SHA, GxsnmpSha))
#define GXSNMP_SHA_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GXSNMP_TYPE_SHA, GxsnmpShaClass))
#define GXSNMP_IS_SHA(object)          (G_TYPE_CHECK_INSTANCE_TYPE ((object), GXSNMP_TYPE_SHA))
#define GXSNMP_IS_SHA_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_SHA))
#define GXSNMP_SHA_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GXSNMP_TYPE_SHA, GxsnmpShaClass))


struct _GxsnmpSha
{
  GxsnmpDigest parent_instance;

  /*< public >*/

  /*< private >*/
};

struct _GxsnmpShaClass
{
  GxsnmpDigestClass parent_class;

};

GType        gxsnmp_sha_get_type (void) G_GNUC_CONST;

GxsnmpSha*   gxsnmp_sha_new	 ();


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
