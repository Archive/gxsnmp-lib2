/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 */
#include <glib.h>
#include "gxsnmp_message.h"

void
gxsnmp_io_message_init (GxsnmpIOMessage *message)
{
  message->message_flags = 0;
  message->ref_count = 1;
}

void 
gxsnmp_io_message_ref (GxsnmpIOMessage *message)
{
  g_return_if_fail (message != NULL);

  message->ref_count++;
}

void 
gxsnmp_io_message_unref (GxsnmpIOMessage *message)
{
  g_return_if_fail (message != NULL);

  message->ref_count--;
  if (message->ref_count == 0)
    message->funcs->io_free (message);
}

GIOError 
gxsnmp_io_message_recv (GxsnmpIOMessage  *message, 
		 	gchar           **buf, 
			guint  		 *count,
		   	GxsnmpIOAddress   sender)
{
  g_return_val_if_fail (message != NULL, G_IO_ERROR_UNKNOWN);

  return message->funcs->io_recv (message, buf, count, sender);
}

GIOError 
gxsnmp_io_message_send (GxsnmpIOMessage *message, 
		   	gchar           *buf, 
		   	guint            count,
		   	GxsnmpIOAddress  recipient)
{
  g_return_val_if_fail (message != NULL, G_IO_ERROR_UNKNOWN);

  return message->funcs->io_send (message, buf, count, recipient);
}

void
gxsnmp_io_message_close (GxsnmpIOMessage *message)
{
  g_return_if_fail (message != NULL);

  message->funcs->io_close (message);
}

GSource *
gxsnmp_io_create_watch (GxsnmpIOMessage *message, GIOCondition condition)
{
  g_return_val_if_fail (message != NULL, NULL);

  return message->funcs->io_create_watch (message, condition);
}

guint 
gxsnmp_io_madd_watch_full (GxsnmpIOMessage    *message,
		      	   gint                priority,
		      	   GIOCondition        condition,
		      	   GxsnmpIOMFunc       func,
		      	   gpointer            user_data,
		      	   GDestroyNotify      notify)
{
  GSource *source;
  guint id;

  g_return_val_if_fail (message != NULL, 0);

  source = gxsnmp_io_create_watch (message, condition);
  
  if (priority != G_PRIORITY_DEFAULT)
    g_source_set_priority (source, priority);
  g_source_set_callback (source, (GSourceFunc)func, user_data, notify);

  id = g_source_attach (source, NULL);
  g_source_unref (source);

  return id;
}

guint 
gxsnmp_io_madd_watch (GxsnmpIOMessage    *message,
		      GIOCondition        condition,
		      GxsnmpIOMFunc       func,
		      gpointer            user_data)
{
  return gxsnmp_io_madd_watch_full (message, G_PRIORITY_DEFAULT, condition,
		 		    func, user_data, NULL);
}
